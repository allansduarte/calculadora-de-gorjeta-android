package com.example.allan.ex1.controller;

import android.content.Context;

import com.example.allan.ex1.view.MainView;

/**
 * Created by allan on 15/07/15.
 */
public class MainController {

    private MainView mainView;
    private double currentBillTotal;
    private int currentCustomPercent;

    public MainController()
    {
    }

    public MainController(MainView mainView)
    {
        this.mainView = mainView;
    }

    public MainView getMainView() {
        return mainView;
    }

    public void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public double getCurrentBillTotal() {
        return currentBillTotal;
    }

    public void setCurrentBillTotal(double currentBillTotal) {
        this.currentBillTotal = currentBillTotal;
    }

    public int getCurrentCustomPercent() {
        return currentCustomPercent;
    }

    public void setCurrentCustomPercent(int currentCustomPercent) {
        this.currentCustomPercent = currentCustomPercent;
    }

    //atualiza os componentes EditText de gorjeta de 10, 15 e 20%
    public void updateStandart()
    {
        double tenPercentTip    = this.currentBillTotal * .1;
        double tenPercentTotal  = this.currentBillTotal + tenPercentTip;

        mainView.getTip10EditText().setText(String.format("%.02f", tenPercentTip));
        mainView.getTotal10EditText().setText(String.format("%.02f", tenPercentTotal));

        double fifteenPercentTip    = this.currentBillTotal * .15;
        double fifteenPercentTotal  = this.currentBillTotal + fifteenPercentTip;

        mainView.getTip15EditText().setText(String.format("%.02f", fifteenPercentTip));
        mainView.getTotal15EditText().setText(String.format("%.02f", fifteenPercentTotal));

        double twentyPercentTip     = this.currentCustomPercent * .20;
        double twentyPercentTotal   = this.currentBillTotal + twentyPercentTip;

        mainView.getTip20EditText().setText(String.format("%.02f", twentyPercentTip));
        mainView.getTotal20EditText().setText(String.format("%.02f", twentyPercentTotal));
    }

    //atualiza os componentes EditText de gorjeta personalizada e total
    public void updateCustom()
    {
        mainView.getCustomTipTextView().setText(this.currentCustomPercent + "%");

        double customTipAmount      = this.currentBillTotal * this.currentCustomPercent * 0.1;
        double customTotalAmount    = this.currentBillTotal + customTipAmount;

        mainView.getTipCustomEditText().setText(String.format("%.02f", customTipAmount));
        mainView.getTotalCustomEditText().setText(String.format("%.02f", customTotalAmount));
    }
}
