package com.example.allan.ex1.view;

import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by allan on 15/07/15.
 */
public class MainView {
    private EditText tip10EditText;
    private EditText total10EditText;
    private EditText tip15EditText;
    private EditText total15EditText;
    private EditText billEditText;
    private EditText tip20EditText;
    private EditText total20EditText;
    private TextView customTipTextView;
    private EditText tipCustomEditText;
    private EditText totalCustomEditText;
    private SeekBar customSeekBar;

    public EditText getTip10EditText() {
        return tip10EditText;
    }

    public void setTip10EditText(EditText tip10EditText) {
        this.tip10EditText = tip10EditText;
    }

    public EditText getTotal10EditText() {
        return total10EditText;
    }

    public void setTotal10EditText(EditText total10EditText) {
        this.total10EditText = total10EditText;
    }

    public EditText getTip15EditText() {
        return tip15EditText;
    }

    public void setTip15EditText(EditText tip15EditText) {
        this.tip15EditText = tip15EditText;
    }

    public EditText getTotal15EditText() {
        return total15EditText;
    }

    public void setTotal15EditText(EditText total15EditText) {
        this.total15EditText = total15EditText;
    }

    public EditText getBillEditText() {
        return billEditText;
    }

    public void setBillEditText(EditText billEditText) {
        this.billEditText = billEditText;
    }

    public EditText getTip20EditText() {
        return tip20EditText;
    }

    public void setTip20EditText(EditText tip20EditText) {
        this.tip20EditText = tip20EditText;
    }

    public EditText getTotal20EditText() {
        return total20EditText;
    }

    public void setTotal20EditText(EditText total20EditText) {
        this.total20EditText = total20EditText;
    }

    public TextView getCustomTipTextView() {
        return customTipTextView;
    }

    public void setCustomTipTextView(TextView customTipTextView) {
        this.customTipTextView = customTipTextView;
    }

    public EditText getTipCustomEditText() {
        return tipCustomEditText;
    }

    public void setTipCustomEditText(EditText tipCustomEditText) {
        this.tipCustomEditText = tipCustomEditText;
    }

    public EditText getTotalCustomEditText() {
        return totalCustomEditText;
    }

    public void setTotalCustomEditText(EditText totalCustomEditText) {
        this.totalCustomEditText = totalCustomEditText;
    }

    public SeekBar getCustomSeekBar() {
        return customSeekBar;
    }

    public void setCustomSeekBar(SeekBar customSeekBar) {
        this.customSeekBar = customSeekBar;
    }
}
