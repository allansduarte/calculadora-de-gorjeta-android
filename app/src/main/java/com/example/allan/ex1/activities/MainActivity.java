package com.example.allan.ex1.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.example.allan.ex1.R;
import com.example.allan.ex1.controller.MainController;
import com.example.allan.ex1.view.MainView;

public class MainActivity extends ActionBarActivity {

    private static final String BILL_TOTAL = "BILL_TOTAL";
    private static final String CUSTOM_PERCENT = "CUSTOM_PERCENT";

    public MainView mainView;
    public MainController mainController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainController = new MainController();

        if( savedInstanceState == null ){
            mainController.setCurrentBillTotal(0.0);
            mainController.setCurrentCustomPercent(18);
        }
        else{
            mainController.setCurrentBillTotal(savedInstanceState.getDouble(BILL_TOTAL));
            mainController.setCurrentCustomPercent(savedInstanceState.getInt(CUSTOM_PERCENT));
        }

        mainView = new MainView();
        initViews();

        mainView.getBillEditText().addTextChangedListener(billEditTextWatcher);
        mainView.getCustomSeekBar().setOnSeekBarChangeListener(customSeekBarListener);

        mainController.setMainView(mainView);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putDouble(BILL_TOTAL, mainController.getCurrentBillTotal());
        outState.putInt(CUSTOM_PERCENT, mainController.getCurrentCustomPercent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews(){
        mainView.setTip10EditText((EditText) findViewById(R.id.tip10EditText));
        mainView.setTotal10EditText((EditText) findViewById(R.id.total10EditText));
        mainView.setTip15EditText((EditText) findViewById(R.id.tip15EditText));
        mainView.setTotal15EditText((EditText) findViewById(R.id.total15EditText));
        mainView.setTip20EditText((EditText) findViewById(R.id.tip20EditText));
        mainView.setTotal20EditText((EditText) findViewById(R.id.total20EditText));
        mainView.setCustomTipTextView((TextView) findViewById(R.id.customTipTextView));
        mainView.setTipCustomEditText((EditText) findViewById(R.id.tipCustomEditText));
        mainView.setTotalCustomEditText((EditText) findViewById(R.id.totalCustomEditText));
        mainView.setBillEditText((EditText) findViewById(R.id.billEditText));
        mainView.setCustomSeekBar((SeekBar) findViewById(R.id.customSeekBar));
    }

    //chamado quando o usuário muda a posição do SeekBar
    private OnSeekBarChangeListener customSeekBarListener = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            mainController.setCurrentCustomPercent(seekBar.getProgress());
            mainController.updateCustom();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    //objeto de tratamento de eventos que responde aos eventos de billEditText
    private TextWatcher billEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                mainController.setCurrentBillTotal(Double.parseDouble(s.toString()));
            } catch(NumberFormatException e){
                mainController.setCurrentBillTotal(0.0);
            }

            mainController.updateStandart();
            mainController.updateCustom();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
}
