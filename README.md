# Calculadora de Gorjeta #

Este app não é de lógica ou recursos complexos, mas com o objetivo de demonstrar uma arquitetura de código de um App simples, que ainda não utilize persistências e comunicações.

O aplicativo demonstra de maneira muito simples como aplicar uma arquitetura - em ambiente Android - que é totalmente desacoplada da lógica de negócio, manipulação de Views Objects e a maneira correta de dar o dispatch, utilizando um View Object.

### Descrição da arquitetura ###

A arquitetura utiliza o conceito de Activities, Views e Controller, pois como o aplicativo não possui funcionalidades de persistências e comunicações, foi demonstrado somente como tratar e desacoplar a lógica e visão.

### Vantagens da arquitetura ###

* Divisões de tarefas entre profissionais;
* Otimização na manutenção de uma feature ou customização;
* Fácil manutenção de toda a tela ou aplicação;
* Praticidade no desenvolvimento, ou seja, sem dificuldades de novos desenvolvedores entenderem o código fonte para atuar no projeto;
* A qualidade é divida, ou seja, é o chamado ditado "dividir para conquistar" para que seja possível cobrir o código fonte necessário, caso seja um objetivo;
* Testes automatizados melhores.

### Desvantagens ###

* O aplicativo fica mais fragmentado e sensível a erros.